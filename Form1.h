#pragma once

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  pole11;
	protected:
	private: System::Windows::Forms::Button^  pole12;
	private: System::Windows::Forms::Button^  pole21;
	private: System::Windows::Forms::Button^  pole31;
	private: System::Windows::Forms::Button^  pole13;
	private: System::Windows::Forms::Button^  pole22;
	private: System::Windows::Forms::Button^  pole23;
	private: System::Windows::Forms::Button^  pole32;
	private: System::Windows::Forms::Button^  pole33;
	private: System::Windows::Forms::MenuStrip^  menu_glowne;

	private: System::Windows::Forms::ToolStripMenuItem^  menu_gra;
	private: System::Windows::Forms::ToolStripMenuItem^  menu_nowagra;
	private: System::Windows::Forms::ToolStripMenuItem^  menu_highscores;
	private: System::Windows::Forms::ToolStripMenuItem^  menu_pomoc;





	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pole11 = (gcnew System::Windows::Forms::Button());
			this->pole12 = (gcnew System::Windows::Forms::Button());
			this->pole21 = (gcnew System::Windows::Forms::Button());
			this->pole31 = (gcnew System::Windows::Forms::Button());
			this->pole13 = (gcnew System::Windows::Forms::Button());
			this->pole22 = (gcnew System::Windows::Forms::Button());
			this->pole23 = (gcnew System::Windows::Forms::Button());
			this->pole32 = (gcnew System::Windows::Forms::Button());
			this->pole33 = (gcnew System::Windows::Forms::Button());
			this->menu_glowne = (gcnew System::Windows::Forms::MenuStrip());
			this->menu_gra = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menu_nowagra = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menu_highscores = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menu_pomoc = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menu_glowne->SuspendLayout();
			this->SuspendLayout();
			// 
			// pole11
			// 
			this->pole11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole11->Location = System::Drawing::Point(17, 31);
			this->pole11->Name = L"pole11";
			this->pole11->Size = System::Drawing::Size(81, 75);
			this->pole11->TabIndex = 0;
			this->pole11->Text = L"1";
			this->pole11->UseVisualStyleBackColor = true;
			// 
			// pole12
			// 
			this->pole12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole12->Location = System::Drawing::Point(104, 31);
			this->pole12->Name = L"pole12";
			this->pole12->Size = System::Drawing::Size(81, 75);
			this->pole12->TabIndex = 1;
			this->pole12->Text = L"2";
			this->pole12->UseVisualStyleBackColor = true;
			// 
			// pole21
			// 
			this->pole21->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole21->Location = System::Drawing::Point(16, 112);
			this->pole21->Name = L"pole21";
			this->pole21->Size = System::Drawing::Size(81, 75);
			this->pole21->TabIndex = 2;
			this->pole21->Text = L"4";
			this->pole21->UseVisualStyleBackColor = true;
			// 
			// pole31
			// 
			this->pole31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole31->Location = System::Drawing::Point(16, 193);
			this->pole31->Name = L"pole31";
			this->pole31->Size = System::Drawing::Size(81, 75);
			this->pole31->TabIndex = 3;
			this->pole31->Text = L"7";
			this->pole31->UseVisualStyleBackColor = true;
			// 
			// pole13
			// 
			this->pole13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole13->Location = System::Drawing::Point(191, 31);
			this->pole13->Name = L"pole13";
			this->pole13->Size = System::Drawing::Size(81, 75);
			this->pole13->TabIndex = 4;
			this->pole13->Text = L"3";
			this->pole13->UseVisualStyleBackColor = true;
			// 
			// pole22
			// 
			this->pole22->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole22->Location = System::Drawing::Point(103, 112);
			this->pole22->Name = L"pole22";
			this->pole22->Size = System::Drawing::Size(81, 75);
			this->pole22->TabIndex = 5;
			this->pole22->Text = L"5";
			this->pole22->UseVisualStyleBackColor = true;
			// 
			// pole23
			// 
			this->pole23->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole23->Location = System::Drawing::Point(191, 112);
			this->pole23->Name = L"pole23";
			this->pole23->Size = System::Drawing::Size(81, 75);
			this->pole23->TabIndex = 6;
			this->pole23->Text = L"6";
			this->pole23->UseVisualStyleBackColor = true;
			// 
			// pole32
			// 
			this->pole32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole32->Location = System::Drawing::Point(104, 193);
			this->pole32->Name = L"pole32";
			this->pole32->Size = System::Drawing::Size(81, 75);
			this->pole32->TabIndex = 7;
			this->pole32->Text = L"8";
			this->pole32->UseVisualStyleBackColor = true;
			// 
			// pole33
			// 
			this->pole33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->pole33->Location = System::Drawing::Point(191, 193);
			this->pole33->Name = L"pole33";
			this->pole33->Size = System::Drawing::Size(81, 75);
			this->pole33->TabIndex = 8;
			this->pole33->UseVisualStyleBackColor = true;
			// 
			// menu_glowne
			// 
			this->menu_glowne->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->menu_gra, this->menu_highscores,
					this->menu_pomoc
			});
			this->menu_glowne->Location = System::Drawing::Point(0, 0);
			this->menu_glowne->Name = L"menu_glowne";
			this->menu_glowne->Size = System::Drawing::Size(284, 24);
			this->menu_glowne->TabIndex = 9;
			this->menu_glowne->Text = L"menu_glowne";
			// 
			// menu_gra
			// 
			this->menu_gra->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->menu_nowagra });
			this->menu_gra->Name = L"menu_gra";
			this->menu_gra->Size = System::Drawing::Size(37, 20);
			this->menu_gra->Text = L"Gra";
			// 
			// menu_nowagra
			// 
			this->menu_nowagra->Name = L"menu_nowagra";
			this->menu_nowagra->Size = System::Drawing::Size(152, 22);
			this->menu_nowagra->Text = L"Nowa Gra";
			// 
			// menu_highscores
			// 
			this->menu_highscores->Name = L"menu_highscores";
			this->menu_highscores->Size = System::Drawing::Size(82, 20);
			this->menu_highscores->Text = L"High Scores";
			// 
			// menu_pomoc
			// 
			this->menu_pomoc->Name = L"menu_pomoc";
			this->menu_pomoc->Size = System::Drawing::Size(57, 20);
			this->menu_pomoc->Text = L"Pomoc";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 280);
			this->Controls->Add(this->pole33);
			this->Controls->Add(this->pole32);
			this->Controls->Add(this->pole23);
			this->Controls->Add(this->pole22);
			this->Controls->Add(this->pole13);
			this->Controls->Add(this->pole31);
			this->Controls->Add(this->pole21);
			this->Controls->Add(this->pole12);
			this->Controls->Add(this->pole11);
			this->Controls->Add(this->menu_glowne);
			this->MainMenuStrip = this->menu_glowne;
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->menu_glowne->ResumeLayout(false);
			this->menu_glowne->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}

